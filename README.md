#Demo project for RSS integration.#

This project includes:

* [Spring Boot](https://projects.spring.io/spring-boot/)
* [HATEOAS](https://spring.io/understanding/HATEOAS) (Hypermedia as the Engine of Application State) out of the box.
* [Thymeleaf](http://www.thymeleaf.org/) templates, [Webjars](http://www.webjars.org/)
* [Rome](https://rometools.github.io/rome/) for RSS feed
* [H2](http://www.h2database.com/html/main.html) in-memory DB & [Hibernate](http://hibernate.org/orm/) ORM
* [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Mockito](http://site.mockito.org/) & [JUnit](http://junit.org/junit4/)

Run:
```
$mvn spring-boot:run
```
Test:
```
$mvn test
```