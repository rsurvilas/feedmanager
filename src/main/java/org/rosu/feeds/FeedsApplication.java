package org.rosu.feeds;

import com.sun.syndication.io.SyndFeedInput;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FeedsApplication {

    public static void main(String[] args) {
        SpringApplication.run(FeedsApplication.class, args);
    }

    @Bean(name = "syndFeedInput")
    public SyndFeedInput getSyndFeedInput() {
        return new SyndFeedInput();
    }

}
