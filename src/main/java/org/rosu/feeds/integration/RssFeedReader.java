package org.rosu.feeds.integration;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by bromas on 11/6/16.
 */
@Service
public class RssFeedReader {
    private static final Logger LOG = LoggerFactory.getLogger(RssFeedReader.class);

    private SyndFeedInput syndFeedInput;
    private URL url = null;

    /**
     * Gets SyndFeed interface for reading feed data.
     *
     * @return SyndFeed implementation, null if URL is not valid or there is some IOException during data retrieval.
     */
    public SyndFeed getSyndFeed(String url) {
        setUrl(url);
        SyndFeed feed = null;

        if (getUrl() != null) {
            try {
                feed = syndFeedInput.build(new InputStreamReader(getUrl().openStream()));
            } catch (IOException | FeedException ioe) {
                LOG.error("Feed exception: {}", ioe.getMessage());
            }
        }

        return feed;
    }

    public URL getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        try {
            this.url = new URL(url);
        } catch (MalformedURLException mue) {
            LOG.error("Malformed URL: {}", mue.getMessage());
        }
    }

    @Autowired
    public void setSyndFeedInput(SyndFeedInput syndFeedInput) {
        this.syndFeedInput = syndFeedInput;
    }
}
