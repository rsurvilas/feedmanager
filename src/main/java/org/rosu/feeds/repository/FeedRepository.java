package org.rosu.feeds.repository;

import org.rosu.feeds.entity.Feed;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by rsurvilas on 2016.11.02.
 *
 * This is RESTful repository. It is based on HATEOAS (Hypermedia as the Engine
 * of Application State) is a constraint of the REST application architecture.
 * A hypermedia-driven site provides information to navigate the site's REST
 * interfaces dynamically by including hypermedia links with the responses.
 *
 * This kind of repository enables CRUD operations through REST, adds useful
 * stuff for Javascript based front ends.
 */
@RepositoryRestResource(collectionResourceRel = "rest-feeds", path = "rest-feeds")
public interface FeedRepository extends CrudRepository<Feed, Long> {
}
