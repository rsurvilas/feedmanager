package org.rosu.feeds.repository;

import org.rosu.feeds.entity.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by bromas on 11/7/16.
 */
@RepositoryRestResource(collectionResourceRel = "rest-items", path = "rest-items")
public interface ItemRepository extends CrudRepository<Item, Long> {
}
