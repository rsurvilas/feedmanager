package org.rosu.feeds;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

/**
 * Created by bromas on 11/5/16.
 *
 * THYMELEAF CONFIGURATION
 * TemplateResolver <- TemplateEngine <- ViewResolver
 */
@Configuration
public class ThymeleafConfiguration extends WebMvcConfigurerAdapter {

/*    private ApplicationContext applicationContext;

    @Bean(name = "templateResolver")
    public ServletContextTemplateResolver getTemplateResolver() {
        ServletContextTemplateResolver tr = new ServletContextTemplateResolver();
        tr.setPrefix("/WEB-INF/templates/");
        tr.setSuffix(".html");
        tr.setTemplateMode("HTML5");

        return tr;
    }

    @Bean(name = "templateEngine")
    public SpringTemplateEngine getTemplateEngine() {
        SpringTemplateEngine te = new SpringTemplateEngine();
        te.setTemplateResolver(getTemplateResolver());

        return te;
    }

    @Bean(name = "viewResolver")
    public ThymeleafViewResolver getViewResolver() {
        ThymeleafViewResolver vr = new ThymeleafViewResolver();
        vr.setTemplateEngine(getTemplateEngine());

        return vr;
    }*/

    @Bean(name = "messageSource")
    public ResourceBundleMessageSource getMessageSource() {
        ResourceBundleMessageSource rb = new ResourceBundleMessageSource();
        rb.setBasename("Messages");
        return rb;
    }

/*    @Override
    public void setApplicationContext(final ApplicationContext applicationContext)
            throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        super.addResourceHandlers(registry);
        //registry.addResourceHandler("/images*//**").addResourceLocations("/images/");
        registry.addResourceHandler("/css*//**").addResourceLocations("/css/");
        //registry.addResourceHandler("/js*//**").addResourceLocations("/js/");
    }*/

}
