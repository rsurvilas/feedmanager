package org.rosu.feeds.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by rsurvilas on 2016.11.02.
 */
@Entity
public class Item {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String link;
    private String title;

    @Temporal(TemporalType.DATE)
    private Date published;
    @Length(max = 100000)
    private String description;

    public Item() {}

    public Item(String link, String title, Date published, String description) {
        this.link = link;
        this.title = title;
        this.published = published;
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPublished() {
        return published;
    }

    public void setPublished(Date published) {
        this.published = published;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("Item[title='%s', link='%s', published='%s', descr='%s']",
                this.title, this.link, this.published, this.description);
    }
}
