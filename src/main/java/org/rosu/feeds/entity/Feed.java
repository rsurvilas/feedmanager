package org.rosu.feeds.entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.web.PageableDefault;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by rsurvilas on 2016.11.02.
 */
@Entity
public class Feed {
    private static final Logger log = LoggerFactory.getLogger(Feed.class);

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String url;
    private String title;

    @Temporal(TemporalType.DATE)
    private Date lastUpdate;
    private String feedName;

    @ElementCollection(targetClass=Item.class)
    private List<Item> items = new ArrayList<>();;


    /**
     * This constructor is essential! Without it Spring doesn't know which to
     * choose when creating this object.
     * */
    protected Feed() {}

    public Feed(String url, String title, Date lastUpdate, String feedName) {
        this.url = url;
        this.title = title;
        this.lastUpdate = lastUpdate;
        this.feedName = feedName;
    }
    public Long getId() {
        return this.id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getFeedName() {
        return feedName;
    }

    public void setFeedName(String feedName) {
        this.feedName = feedName;
    }

    /*
    * By specifying the CascadeType.All hibernate saves list to the DB when saving Feed.
    * */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "feed", cascade = CascadeType.ALL)
    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
