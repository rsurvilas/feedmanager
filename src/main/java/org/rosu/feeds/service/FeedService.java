package org.rosu.feeds.service;

import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import org.rosu.feeds.entity.Feed;
import org.rosu.feeds.entity.Item;
import org.rosu.feeds.integration.RssFeedReader;
import org.rosu.feeds.repository.FeedRepository;
import org.rosu.feeds.repository.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by bromas on 11/5/16.
 */
@Service
public class FeedService {
    private static final Logger LOG = LoggerFactory.getLogger(FeedService.class);

    private FeedRepository feedRepository;
    private ItemRepository itemRepository;
    private RssFeedReader rssFeedReader;

    /**
     * Finds all Feeds.
     *
     * @return Iterable object of Feeds.
     * */
    public Iterable<Feed> findAll() {
        return this.feedRepository.findAll();
    }

    /**
     * Finds Feed by ID.
     *
     *  @param idStr ID String.
     *
     * @return Feed object from DB if given ID string is successfully parsed
     * from String. Otherwise null.
     * */
    public Feed findById(String idStr){
        try {
            return feedRepository.findOne(Long.valueOf(idStr));
        } catch (NumberFormatException e) {
            LOG.error("Couldn't parse id: {}", idStr);
            return null;
        }
    }

    /**
     * Saves feed to DB. Before actual saving it retrieves all feed entries by
     * a given URL, saves them and finally saves itself.
     *
     * @param feed Feed to save.
     * */
    public void saveFeed(final Feed feed) {
        setFeedItems(feed);
        List<Item> feedItems = feed.getItems();
        feedItems.forEach(entry -> this.itemRepository.save(entry));
        this.feedRepository.save(feed);
    }

    /**
     * Sets feeds items according feed url.
     * */
    private void setFeedItems(Feed feed) {
        SyndFeed syndFeed = rssFeedReader.getSyndFeed(feed.getUrl());
        if (syndFeed != null) {
            feed.setTitle(syndFeed.getTitle());
            feed.setFeedName(syndFeed.getDescription());
            feed.setLastUpdate(syndFeed.getPublishedDate());

            List<Item> feedItems = feed.getItems();
            syndFeed.getEntries().forEach(entry -> feedItems.add(buildItem(entry)));
        }
    }

    /**
     * Extracts data from SyndEntry and creates Item object.
     *
     * @param entry RSS feed entry a.k.a SyndEntry.
     *
     * @return Item object if given entry is an instance of SyndEntryImpl. Null
     * is returned otherwise.
     * */
    private Item buildItem(Object entry) {
        Item item = null;
        if (entry instanceof SyndEntryImpl) {
            SyndEntryImpl syndEntry = (SyndEntryImpl)entry;

            item = new Item(syndEntry.getLink(),
                    syndEntry.getTitle(),
                    syndEntry.getPublishedDate() == null ? syndEntry.getUpdatedDate() : null,
                    getDescription(syndEntry));
        }
        return item;
    }

    /**
     * This method returns available description.
     *
     * @param syndEntry Description source.
     *
     * @return First it tries to get it from SyndEntity.getDescription() method.
     * If it returns null, this method tries to get it from
     * SyndEntity.getContents() method. If this fails too, method returns null.
     * */
    private String getDescription(SyndEntryImpl syndEntry) {
        SyndContent description = syndEntry.getDescription();
        List<SyndContent> contentList = syndEntry.getContents();

        if (description != null) {
            return description.getValue();
        } else if (contentList != null && !contentList.isEmpty()) {
            String commaSeparatedContents = contentList.stream().map(
                    i -> i.getValue()).collect(Collectors.joining());
            return commaSeparatedContents;
        } else {
            return null;
        }
    }

    @Autowired
    public void setRssFeedReader(RssFeedReader rssFeedReader) {
        this.rssFeedReader = rssFeedReader;
    }

    @Autowired
    public void setFeedRepository(FeedRepository feedRepository) {
        this.feedRepository = feedRepository;
    }

    @Autowired
    public void setItemRepository(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }
}
