package org.rosu.feeds.web;

import org.rosu.feeds.entity.Feed;
import org.rosu.feeds.service.FeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FeedController {
    private FeedService feedService;

    @RequestMapping("/")
    public String showFeeds() {
        return "feeds";
    }

    @RequestMapping("/newFeed")
    public String newFeed(final Feed feed) {
        return "../fragments/newFeed::newFeed";
    }

    @RequestMapping(value = "/saveFeed", params = {"save"})
    public String saveFeed(final Feed feed,
                          final BindingResult bindingResult,
                          final ModelMap model) {
        if (bindingResult.hasErrors()) {
            return "/";
        }
        this.feedService.saveFeed(feed);
        model.clear();
        return "redirect:/";
    }

    @RequestMapping(value = "/viewFeed/{id}")
    public String findFeedById(Model model, @PathVariable("id") String idStr) {
        model.addAttribute("feed", this.feedService.findById(idStr));
        return "../fragments/viewFeed::viewFeed";
    }

    @ModelAttribute("allFeeds")
    public Iterable<Feed> populateFeeds() {
        return this.feedService.findAll();
    }

    @Autowired
    public void setFeedService(FeedService feedService) {
        this.feedService = feedService;
    }
}