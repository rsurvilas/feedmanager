package org.rosu.feeds.integration;

import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.rosu.feeds.entity.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

/**
 * Created by rsurvilas on 2016.11.07.
 */
@RunWith(JUnit4.class)
public class RssFeedReaderTest {
    private static final Logger LOG = LoggerFactory.getLogger(RssFeedReader.class);
    private static final String URL = "http://15min.lt/rss";
    private static final String URL2 = "https://spring.io/blog/category/news.atom";

    private RssFeedReader rssFeedReader;

    @Before
    public void init() {
        rssFeedReader = new RssFeedReader();
        rssFeedReader.setSyndFeedInput(new SyndFeedInput());
    }

    @Test
    public void feedReaderLoads() throws Exception {
        assertNotNull(rssFeedReader);
    }

    @Test
    public void getSyndFeedSuccess() {
        SyndFeed syndFeed = rssFeedReader.getSyndFeed(URL2);
        assertNotNull(syndFeed);
        LOG.info("Desc.: {}", syndFeed.getDescription());
        LOG.info("Link: {}", syndFeed.getLink());
        LOG.info("Title: {}", syndFeed.getTitle());
        LOG.info("Entries: {}", syndFeed.getEntries().size());
    }
}